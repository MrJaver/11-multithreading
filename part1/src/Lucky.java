import java.util.Optional;

public class Lucky {

    static Optional<Integer> x = Optional.of(0);
    //один из вариантов, который я придумал, это занесение x в какой-нибудь объект, к примеру Optional,
    // который будет защищаться монитором, иначе будет проблема обращения разными потоками к одним и тем же данным (x)
    static int count = 0;

    //Оптимальным решением будет класс-обёртка AtomicInteger, реализующий это по умолчанию (без synchronized)

    static class LuckyThread extends Thread {
        @Override
        public void run() {
            synchronized (x) {

                int xpart = x.get();
                while (xpart < 999999) {
                    xpart++;
                    if ((xpart % 10) + (xpart / 10) % 10 + (xpart / 100) % 10 == (xpart / 1000)
                            % 10 + (xpart / 10000) % 10 + (xpart / 100000) % 10) {
                        System.out.println(xpart);
                        count++;
                    }
                }
                x = Optional.of(xpart);
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new LuckyThread();
        Thread t2 = new LuckyThread();
        Thread t3 = new LuckyThread();
        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println("Total: " + count);
    }
}